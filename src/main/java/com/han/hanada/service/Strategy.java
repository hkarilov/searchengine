package com.han.hanada.service;


import com.han.hanada.model.data.Data;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface Strategy {
    void search(String query) throws IOException;
    Map<String, List<? extends Data>> setData();
    Document getDocument(String searchEngine, String query) throws IOException;
}
