package com.han.hanada.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.han.hanada.model.weather.Metric;
import com.han.hanada.model.weather.Tempurature;
import com.han.hanada.model.weather.Weather;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Service
public class WeatherService {

    private static final String url = "http://apidev.accuweather.com/currentconditions/v1/222844.json?language=en&apikey=hoArfRosT1215";

    private JSONObject getJsonTree() throws IOException{
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response
                = restTemplate.getForEntity(url, String.class);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());

        JsonNode jsonNode = root.path(0);

        return new JSONObject(jsonNode.toString());
    }

    public Weather getWeather() throws IOException{
        JSONObject jsonObject = getJsonTree();

        return new Weather(jsonObject.getString("WeatherText"), jsonObject.getInt("WeatherIcon"),
                jsonObject.getString("Link"),
                new Tempurature(
                        new Metric(jsonObject.getJSONObject("Temperature").getJSONObject("Metric").getDouble("Value"), "C"),
                        new Metric(jsonObject.getJSONObject("Temperature").getJSONObject("Imperial").getDouble("Value"), "F")
        ));
    }

}
