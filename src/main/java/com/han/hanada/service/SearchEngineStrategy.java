package com.han.hanada.service;

import com.han.hanada.model.data.Bing;
import com.han.hanada.model.data.Data;
import com.han.hanada.model.data.Google;
import com.han.hanada.model.data.Rambler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;

@Component("searchEngineStrategy")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)

public class SearchEngineStrategy implements Strategy {

    private final String GOOGLE = "https://www.google.com/search?q=";
    private final String RAMBLER = "https://nova.rambler.ru/search?query=google";
    private final String BING = "https://www.bing.com/search?q=";

    private List<Google> googleDataList;
    private List<Rambler> ramblerDataList;
    private List<Bing> bingDataList;

    private Document googleDocument;
    private Document ramblerDocument;
    private Document bingDocument;

    public SearchEngineStrategy() {
        this.googleDataList = new ArrayList<Google>();
        this.ramblerDataList = new ArrayList<Rambler>();
        this.bingDataList = new ArrayList<Bing>();
    }

    public void search(String query) throws IOException{
        this.googleDocument = getDocument(GOOGLE, query);
        this.ramblerDocument = getDocument(RAMBLER, query);
        this.bingDocument = getDocument(BING, query);
    }

    public Map<String, List<? extends Data>> setData() {
        Elements googleElements = googleDocument.select("div.g").select("h3.r");
        Elements ramblerElements = ramblerDocument.select("div.b-serp-list").select("div.b-serp-item");
        Elements bingElements = bingDocument.select("li.b_algo").select("h2");

        for (Element element : googleElements) {
            Google google = new Google();
            google.setSearchEngine("GOOGLE");
            google.setTitle(element.select("h3.r a").text());
            google.setUrl(element.select("h3.r a").attr("href"));
            googleDataList.add(google);
        }


        for (Element element : ramblerElements) {
            Rambler rambler = new Rambler();
            rambler.setSearchEngine("RAMBLER");
            rambler.setTitle(element.select("a").text());
            rambler.setUrl(element.select("a").attr("href"));
            ramblerDataList.add(rambler);
        }

        for (Element element : bingElements) {
            Bing bing = new Bing();
            bing.setSearchEngine("BING");
            bing.setTitle(element.select("a").text());
            bing.setUrl(element.select("a").attr("href"));
            bingDataList.add(bing);
        }

        Map<String, List<? extends Data>> searchResults = new LinkedHashMap<String, List<? extends Data>>();
        searchResults.put("GOOGLE", googleDataList);
        searchResults.put("BING", bingDataList);
        searchResults.put("RAMBLER", ramblerDataList);

        return searchResults;
    }

    public Document getDocument(String searchEngine, String query) throws IOException {
        return Jsoup.connect(searchEngine + URLEncoder.encode(query))
                .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0")
                .get();

    }
}
