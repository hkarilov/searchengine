package com.han.hanada.service;

import com.han.hanada.model.currency.Currency;
import com.han.hanada.model.currency.Valute;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.List;

@Service
public class CurrencyService {

    private static final String url = "http://www.cbr.ru/scripts/XML_daily_eng.asp";

    private String getUrl(){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        return response.getBody();
    }

    public Currency getCurrency() throws JAXBException {
        StringReader reader = new StringReader(getUrl());
        JAXBContext context = JAXBContext.newInstance(Currency.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Currency currency = (Currency) unmarshaller.unmarshal(reader);
        return currency;
    }
}
