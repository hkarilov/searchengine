package com.han.hanada.controller;


import com.han.hanada.model.currency.Currency;
import com.han.hanada.model.currency.Valute;
import com.han.hanada.service.CurrencyService;
import com.han.hanada.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.text.DecimalFormat;

@Controller
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private CurrencyService currencyService;

    @RequestMapping(value = "/getWidget", method = RequestMethod.GET)
    public String getWeather(Model model) throws IOException, JAXBException {

        Valute kgs = new Valute();
        Valute usd = new Valute();
        Valute eur = new Valute();

        Currency currency = currencyService.getCurrency();

        for(Valute valute: currency.getValutes()){
            if(valute.getCharCode().equals("KGS")){
                kgs = valute;
            }
            else if(valute.getCharCode().equals("USD")){
                usd = valute;
            }
            else if(valute.getCharCode().equals("EUR")){
                eur = valute;
            }
        }

        double rubToKgs = kgs.getNominal() / Double.parseDouble(kgs.getValue().replace(",", "."));
        usd.setDvalue(resultFormat(usd.getValue(), rubToKgs));
        eur.setDvalue(resultFormat(eur.getValue(), rubToKgs));


        model.addAttribute("weather", weatherService.getWeather());
        model.addAttribute("currency", currency);
        model.addAttribute("kgs", kgs);
        model.addAttribute("usd", usd);
        model.addAttribute("eur", eur);
        return "widgets";
    }

    @RequestMapping(value = "/getXml",method = RequestMethod.GET)
    public @ResponseBody
    Currency handleXMLPostRequest () throws JAXBException {
        Currency currency = this.currencyService.getCurrency();
        return currency;
    }

    private double resultFormat (String valCode, double multyplyer) {

        return Double.parseDouble(
                new DecimalFormat("#0.0000").format(
                        Double.parseDouble(valCode.replace(",", ".")) * multyplyer
                )
        );

    }

}
