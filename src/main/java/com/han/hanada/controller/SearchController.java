package com.han.hanada.controller;

import com.han.hanada.service.Strategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;

@Controller
public class SearchController {
    @Autowired
    @Qualifier("searchEngineStrategy")
    private Strategy strategy;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello(){
        return "results";
    }

    @RequestMapping(value = "/results", method = RequestMethod.POST)
    public String getResults(@RequestParam String query, Model model){
        try {
            this.strategy.search(query);
        }catch (IOException e){
            e.printStackTrace();
        }
        model.addAttribute("map_results", this.strategy.setData());
        model.addAttribute("query", query);
        return "results";
    }

}