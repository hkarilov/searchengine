package com.han.hanada.model.data;

public class Rambler extends Data {
    public Rambler(){
        super();
    }
    public Rambler(String searchEngine, String url, String title) {
        super(searchEngine, url, title);
    }
}
