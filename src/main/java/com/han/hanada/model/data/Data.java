package com.han.hanada.model.data;

public class Data {
    private String searchEngine;
    private String url;
    private String title;


    public Data(){
        this.searchEngine = "";
        this.url = "";
        this.title = "";
    }

    public Data(String searchEngine, String url, String title) {
        this.searchEngine = searchEngine;
        this.url = url;
        this.title = title;
    }

    public String getSearchEngine() {
        return searchEngine;
    }

    public void setSearchEngine(String searchEngine) {
        this.searchEngine = searchEngine;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Data{" +
                "searchEngine='" + searchEngine + '\'' +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
