package com.han.hanada.model.data;

public class Google extends Data {
    public Google() {
        super();
    }

    public Google(String searchEngine, String url, String title) {
        super(searchEngine, url, title);
    }
}
