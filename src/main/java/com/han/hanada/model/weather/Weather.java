package com.han.hanada.model.weather;

public class Weather {
    private String weatherText;
    private int weatherIcon;
    private String weatherLink;
    private Tempurature tempurature;

    public Weather(){}

    public Weather(String weatherText, int weatherIcon, String weatherLink, Tempurature tempurature) {
        this.weatherText = weatherText;
        this.weatherIcon = weatherIcon;
        this.weatherLink = weatherLink;
        this.tempurature = tempurature;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public int getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(int weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public String getWeatherLink() {
        return weatherLink;
    }

    public void setWeatherLink(String weatherLink) {
        this.weatherLink = weatherLink;
    }

    public Tempurature getTempurature() {
        return tempurature;
    }

    public void setTempurature(Tempurature tempurature) {
        this.tempurature = tempurature;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "weatherText='" + weatherText + '\'' +
                ", weatherIcon=" + weatherIcon +
                ", tempurature=" + tempurature +
                '}';
    }
}
