package com.han.hanada.model.weather;

public class Metric {
    private double value;
    private String unit;

    public Metric() {
    }

    public Metric(double value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Metric{" +
                "value=" + value +
                ", unit='" + unit + '\'' +
                '}';
    }
}
