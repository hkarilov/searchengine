package com.han.hanada.model.weather;

public class Tempurature {
    private Metric celsius;
    private Metric farengate;

    public Tempurature(){

    }

    public Tempurature(Metric celsius, Metric farengate) {
        this.celsius = celsius;
        this.farengate = farengate;
    }

    public Metric getCelsius() {
        return celsius;
    }

    public void setCelsius(Metric celsius) {
        this.celsius = celsius;
    }

    public Metric getFarengate() {
        return farengate;
    }

    public void setFarengate(Metric farengate) {
        this.farengate = farengate;
    }

    @Override
    public String toString() {
        return "Tempurature{" +
                "celsius=" + celsius +
                ", farengate=" + farengate +
                '}';
    }
}
