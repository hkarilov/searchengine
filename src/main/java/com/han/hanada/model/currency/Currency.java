package com.han.hanada.model.currency;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "ValCurs")
public class Currency {
    private String date;
    private String name;

    private List<Valute> valutes;

    public Currency(){
        this.valutes = new ArrayList<Valute>();
    }

    public String getDate() {
        return date;
    }

    @XmlAttribute(name = "Date")
    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    @XmlAttribute(name = "Name")
    public void setName(String name) {
        this.name = name;
    }

    public List<Valute> getValutes() {
        return valutes;
    }

    @XmlElement(name = "Valute")
    public void setValutes(List<Valute> valutes) {
        this.valutes = valutes;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "date='" + date + '\'' +
                ", name='" + name + '\'' +
                ", valutes=" + valutes +
                '}';
    }
}
