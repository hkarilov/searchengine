<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Widjetcs</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">

</head>
<body>

<div class="container">
    <div class="jumbotron">
        <div class="row">
            <div class="col-xs-5 col-lg-3" style="background: #348E8A;">

            <table style=" color: #EDEDF1">
                <tr>
                    <td colspan="2"><h1>Bishkek</h1></td>
                </tr>
                <tr>
                    <td><h3>${weather.weatherText}</h3></td>
                    <td>
                        <c:choose>
                            <c:when test="${weather.weatherIcon < 10}">
                                <img src="https://developer.accuweather.com/sites/default/files/0${weather.weatherIcon}-s.png">
                            </c:when>
                            <c:otherwise>
                                <img src="https://developer.accuweather.com/sites/default/files/${weather.weatherIcon}-s.png">
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        ${weather.tempurature.celsius.value}<sup>&#9898;</sup>${weather.tempurature.celsius.unit}
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        ${weather.tempurature.farengate.value}<sup>&#9898;</sup>${weather.tempurature.farengate.unit}
                    </td>
                </tr>
            </table>
            </div>

            <div class="col-xs-5 col-lg-3" style="background: #7CB761;">

                <table style="color: #fff;">
                    <tr>
                        <td colspan="3"><h1>Valutes</h1></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">${currency.date}</td>
                    </tr>
                    <tr>
                        <th>USD </th>
                        <td> ${usd.dvalue} KGS</td>
                    </tr>

                    <tr>
                        <th>EURO </th>
                        <td> ${eur.dvalue} KGS</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>
