<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Results</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="container-fluid">
        <a href="/getWidget">Widgets</a>
    <br>
    <h2>Search Engine 3 in 1</h2>

    <form:form action="/results" method="post">
        <input type="search" id="query" name="query" maxlength="256" placeholder="Search in the Web"  size="50" data-reactid="7"
               autocomplete="off" aria-autocomplete="true" aria-controls="searchSuggestionTable" aria-expanded="false"/>
        <input type="submit" value="Search" size="20"/>
    </form:form>
    <br>

    </div>

<c:if test="${!empty map_results}">
    <h2>Results</h2>
    <table class="table table-hover">

        <c:forEach items="${map_results}" var="mapkey">
            <tr>
                <td colspan="3" align="center" style="color: #fff; background: #025aa5"><h3>${mapkey.key}</h3></td>
            </tr>

            <tr>
                <th>ID</th>
                <th>URl</th>
                <th>Link</th>
            </tr>

            <c:forEach items="${mapkey.value}" var="result">
                <c:if test="${mapkey.value.indexOf(result) < 5}">
                    <tr>

                        <td>${mapkey.value.indexOf(result)+1}</td>
                        <td><a href="${result.url}"> ${result.title} </a></td>
                        <td><a href="${result.url}"> ${result.url} </a></td>
                </tr>
                    </c:if>
            </c:forEach>
        </c:forEach>
    </table>

</c:if>
</div>

</body>
</html>
